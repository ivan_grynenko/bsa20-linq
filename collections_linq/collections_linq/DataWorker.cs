﻿using collections_linq.Helpers;
using collections_linq.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace collections_linq
{
    class DataWorker : IDisposable
    {
        private HttpClient client;
        private bool isDisposed = false;
        private string projectsUri;
        private string tasksUri;
        private string teamsnUri;
        private string usersUri;

        public DataWorker()
        {
            client = new HttpClient();            
            client.BaseAddress = new Uri(ConfigurationManager.AppSettings["MainUri"]);
            projectsUri = ConfigurationManager.AppSettings["ProjectsUri"];
            tasksUri = ConfigurationManager.AppSettings["TasksUri"];
            teamsnUri = ConfigurationManager.AppSettings["TeamsnUri"];
            usersUri = ConfigurationManager.AppSettings["UsersUri"];
        }

        public async Task<IEnumerable<Project>> GetAllProjects()
        {
            IEnumerable<Project> projects = null;
            var response = await client.GetAsync(projectsUri);

            if (response.IsSuccessStatusCode)
                projects = await response.Content.ReadAsJsonAsync<IEnumerable<Project>>();

            return projects;
        }

        public async Task<Project> GetProjectById(int id)
        {
            Project project = null;
            var response = await client.GetAsync(projectsUri + $"/{id}");

            if (response.IsSuccessStatusCode)
                project = await response.Content.ReadAsJsonAsync<Project>();

            return project;
        }

        public async Task<IEnumerable<Tasks>> GetAllTasks()
        {
            IEnumerable<Tasks> tasks = null;
            var response = await client.GetAsync(tasksUri);

            if (response.IsSuccessStatusCode)
                tasks = await response.Content.ReadAsJsonAsync<IEnumerable<Tasks>>();

            return tasks;
        }

        public async Task<Tasks> GetTasksById(int id)
        {
            Tasks task = null;
            var response = await client.GetAsync(tasksUri + $"/{id}");

            if (response.IsSuccessStatusCode)
                task = await response.Content.ReadAsJsonAsync<Tasks>();

            return task;
        }

        public async Task<IEnumerable<Team>> GetAllTeams()
        {
            IEnumerable<Team> teams = null;
            var response = await client.GetAsync(teamsnUri);

            if (response.IsSuccessStatusCode)
                teams = await response.Content.ReadAsJsonAsync<IEnumerable<Team>>();

            return teams;
        }

        public async Task<Team> GetTeamById(int id)
        {
            Team team = null;
            var response = await client.GetAsync(teamsnUri + $"/{id}");

            if (response.IsSuccessStatusCode)
                team = await response.Content.ReadAsJsonAsync<Team>();

            return team;
        }

        public async Task<IEnumerable<User>> GetAllUsers()
        {
            IEnumerable<User> users = null;
            var response = await client.GetAsync(usersUri);

            if (response.IsSuccessStatusCode)
                users = await response.Content.ReadAsJsonAsync<IEnumerable<User>>();

            return users;
        }

        public async Task<User> GetUserById(int id)
        {
            User user = null;
            var response = await client.GetAsync(usersUri + $"/{id}");

            if (response.IsSuccessStatusCode)
                user = await response.Content.ReadAsJsonAsync<User>();

            return user;
        }

        public void Dispose()
        {
            Dispose(true);
        }

        private void Dispose(bool disposing)
        {
            if (isDisposed)
                return;

            if (disposing)
                client.Dispose();

            isDisposed = true;
        }
    }
}
