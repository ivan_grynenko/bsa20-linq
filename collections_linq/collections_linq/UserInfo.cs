﻿using collections_linq.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace collections_linq
{
    public class UserInfo
    {
        public User User { get; set; }
        public Project LastProject { get; set; }
        public int? NumberOfTasks { get; set; }
        public int? CanceledOrIncompleteTasks { get; set; }
        public Tasks LongestTask { get; set; }
    }
}
