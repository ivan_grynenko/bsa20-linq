﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace collections_linq.Helpers
{
    public static class HttpContentExtensions
    {
        public static async Task<T> ReadAsJsonAsync<T>(this HttpContent content)
        {
            string json = await content.ReadAsStringAsync();
            T value = JsonConvert.DeserializeObject<T>(json);
            return value;
        }

        //public static async Task<IEnumerable<T>> ReadCollectionAsJsonAsync<T>(this HttpContent content)
        //{
        //    string json = await content.ReadAsStringAsync();
        //    var value = JsonConvert.DeserializeObject<List<T>>(json);
        //    return value;
        //}
    }
}
