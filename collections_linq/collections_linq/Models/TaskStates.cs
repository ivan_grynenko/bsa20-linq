﻿using System;
using System.Collections.Generic;
using System.Text;

namespace collections_linq.Models
{
    public enum TaskStates
    {
        Created,
        Started,
        Finished,
        Canceled
    }
}
