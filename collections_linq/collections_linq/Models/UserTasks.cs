﻿using System;
using System.Collections.Generic;
using System.Text;

namespace collections_linq.Models
{
    public class UserTasks
    {
        public User User { get; set; }
        public IEnumerable<Tasks> Tasks { get; set; }
    }
}
