﻿using collections_linq.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace collections_linq
{
    public class DataService : IDisposable
    {
        private readonly DataWorker dataWorker;
        private bool isDisposed = false;

        public DataService()
        {
            dataWorker = new DataWorker();
        }

        public async Task<Dictionary<string, int>> GetNumberOfTasksByUser(int userId)
        {
            var tasks = await dataWorker.GetAllTasks();

            var result = tasks
                .Where(e => e.PerformerId == userId)
                .GroupBy(e => e.ProjectId)
                .Select(e => new { ProjectId = e.Key, Count = e.Count() })
                .Join(await dataWorker.GetAllProjects(), t => t.ProjectId, p => p.Id, (t, p) => new { p.Name, t.Count })
                .ToDictionary(d => d.Name, d => d.Count);

            return result;
        }

        public async Task<IEnumerable<Tasks>> GetTasksByUserWithMaxNameLength(int userId, int maxLength)
        {
            var tasks = await dataWorker.GetAllTasks();

            var result = tasks
                .Where(e => e.PerformerId == userId && e.Name.Length < maxLength);

            return result;
        }

        public async Task<IEnumerable<Tasks>> GetTasksByUserInCurrentYear(int userId)
        {
            var tasks = await dataWorker.GetAllTasks();

            var result = tasks
                .Where(e => e.PerformerId == userId && e.FinishedAt.Year == DateTime.Now.Year);

            return result;
        }

        public async Task<IEnumerable<(int? Id, string Name, IEnumerable<User> Users)>> GetTeamsOfCertainAge(int minAge)
        {
            var users = await dataWorker.GetAllUsers();

            var result = users
                .GroupBy(e => e.TeamId)
                .Where(g => g.All(e => e.Birthday.AddYears(minAge) <= DateTime.Now) && g.Key != null)
                .Join(await dataWorker.GetAllTeams(), u => u.Key, t => t.Id, (u, t) => new { Id = u.Key, t.Name, Users = u.OrderByDescending(e => e.RegisteredAt).AsEnumerable() })
                .Select(e => (e.Id, e.Name, e.Users));

            return result;
        }

        public async Task<IEnumerable<UserTasks>> GetOrderedUsersWithOrderedTasks()
        {
            var users = await dataWorker.GetAllUsers();

            var result = users
                .Join(await dataWorker.GetAllTasks(), u => u.Id, t => t.PerformerId, (u, t) => new { User = u, Tasks = t })
                .GroupBy(e => e.User)
                .Select(e => new UserTasks { User = e.Key, Tasks = e.Select(t => t.Tasks).OrderByDescending(t => t.Name.Length).AsEnumerable() })
                .OrderBy(e => e.User.FirstName)
                .AsEnumerable();

            return result;
        }

        public async Task<UserInfo> GetUserInfo(int userId)
        {
            var projects = await dataWorker.GetAllProjects();
            var tasks = await dataWorker.GetAllTasks();

            var result = projects            
                .Where(e => e.AuthorId == userId && e.CreatedAt == projects.Max(e => e.CreatedAt))
                .Take(1)
                .Join(await dataWorker.GetAllTasks(), p => p.Id, t => t.ProjectId, (p, t) => new { p, t })
                .GroupBy(e => e.p)
                .Select(e => new
                {
                    LastProject = e.Key,
                    NumOfTasks = e.Count()
                })
                .FirstOrDefault();

            var result2 = tasks
                .Where(e => e.PerformerId == userId)
                .GroupBy(e => e.PerformerId)
                .Select(e => new
                {
                    Incomplete = e.Where(x => x.State == TaskStates.Canceled || x.State == TaskStates.Started).Count(),
                    Longest = e.Where(x => x.FinishedAt - x.CreatedAt == e.Select(x => x.FinishedAt - x.CreatedAt).Max()).FirstOrDefault()
                })
                .FirstOrDefault();

            return new UserInfo
            {
                User = await dataWorker.GetUserById(userId),
                CanceledOrIncompleteTasks = result2?.Incomplete,
                LongestTask = result2?.Longest,
                LastProject = result?.LastProject,
                NumberOfTasks = result?.NumOfTasks
            };
        }

        public async Task<IEnumerable<ProjectInfo>> GetProjectWithTasks()
        {
            var projects = await dataWorker.GetAllProjects();

            var result = projects
                .Join(await dataWorker.GetAllTasks(), p => p.Id, t => t.ProjectId, (p, t) => new { p, t })
                .Join(await dataWorker.GetAllUsers(), e => e.p.TeamId, u => u.TeamId, (e, u) => new { Project = e.p, Task = e.t, User = u })
                .GroupBy(e => e.Project)
                .Select(e => new ProjectInfo
                {
                    Project = e.Key,
                    LongestTask = e.Select(x => x.Task).Where(x => x.Description.Length == e.Select(x => x.Task.Description.Length).Max()).FirstOrDefault(),
                    ShortestTask = e.Select(x => x.Task).Where(x => x.Name.Length == e.Select(x => x.Task.Name.Length).Min()).FirstOrDefault(),
                });

            return result;
        }

        public void Dispose()
        {
            Dispose(true);
        }

        private void Dispose(bool disposing)
        {
            if (isDisposed)
                return;

            if (disposing)
                dataWorker.Dispose();

            isDisposed = true;
        }
    }
}
